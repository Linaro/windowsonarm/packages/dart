import shutil
from os.path import dirname, join

from packagetools.builder import *
from packagetools.command import cygwin_path, run

VERSION = "main"
# first commit to build: 1fed9b5be
RECIPE_DIR = dirname(__file__)


class Recipe(PackageShortRecipe):
    def describe(self) -> PackageInfo:
        return PackageInfo(
            description="Build dart sdk",
            id="dart",
            pretty_name="Dart SDK",
            version=VERSION,
        )

    def all_steps(self, out_dir: str):
        bash = shutil.which("bash")
        out_dir = cygwin_path(out_dir)
        recipe = cygwin_path(join(RECIPE_DIR, "recipe.sh"))
        run(bash, recipe, VERSION, out_dir)


PackageBuilder(Recipe()).make()
